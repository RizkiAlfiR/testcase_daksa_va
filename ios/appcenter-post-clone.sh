#!/usr/bin/env bash
#Place this script in project/ios/
# fail if any command fails
set -e
# debug log
set -x
#sudo gem uninstall cocoapods
#sudo gem install cocoapods -v 1.8.4
echo "uninstalling all cocoapods versions"
sudo gem uninstall cocoapods --all
echo "installing cocoapods version 1.10.0.rc.1"
sudo gem install cocoapods -v 1.10.0.rc.1
pod setup
cd ..
git clone -b master https://github.com/flutter/flutter.git --branch 1.22.1
export PATH=`pwd`/flutter/bin:$PATH
#flutter channel stable
flutter clean
flutter doctor
#flutter version v11.22.1
echo "Installed flutter to `pwd`/flutter"
flutter build ios --release --no-codesign