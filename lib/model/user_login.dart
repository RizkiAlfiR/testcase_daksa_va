class UserLogin {
  String id;
  String accountId;
  String lastLoginTimestamp;
  String sessionStatus;

  UserLogin(
      {this.id, this.accountId, this.lastLoginTimestamp, this.sessionStatus});

  UserLogin.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    accountId = json['accountId'];
    lastLoginTimestamp = json['lastLoginTimestamp'];
    sessionStatus = json['sessionStatus'];
  }

  // Map<String, dynamic> toJson() => {
  //   'id': id,
  //   'accountId': accountId,
  //   'lastLoginTimestamp': lastLoginTimestamp,
  //   'sessionStatus': sessionStatus,
  // };

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['accountId'] = this.accountId;
    data['lastLoginTimestamp'] = this.lastLoginTimestamp;
    data['sessionStatus'] = this.sessionStatus;
    return data;
  }
}
