class UserDetail {
  String id;
  String name;
  String username;
  double balance;

  UserDetail({this.id, this.name, this.username, this.balance = 0});

  UserDetail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    username = json['username'];
    balance = json['balance'];
  }

  // Map<String, dynamic> toJson() => {
  //   'id': id,
  //   'accountId': accountId,
  //   'lastLoginTimestamp': lastLoginTimestamp,
  //   'sessionStatus': sessionStatus,
  // };

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['username'] = this.username;
    data['balance'] = this.balance;
    return data;
  }
}
