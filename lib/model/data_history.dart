class DataHistory {
  String id;
  String inquiryId;
  String accountSrcId;
  String accountDstId;
  String clientRef;
  double amount;
  String transactionTimestamp;

  DataHistory(
      {this.id,
      this.accountSrcId,
      this.accountDstId,
      this.clientRef,
      this.amount = 0,
      this.transactionTimestamp});

  DataHistory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    accountSrcId = json['accountSrcId'];
    accountDstId = json['accountDstId'];
    clientRef = json['clientRef'];
    amount = json['amount'];
    transactionTimestamp = json['transactionTimestamp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['accountSrcId'] = this.accountSrcId;
    data['accountDstId'] = this.accountDstId;
    data['clientRef'] = this.clientRef;
    data['amount'] = this.amount;
    data['transactionTimestamp'] = this.transactionTimestamp;
    return data;
  }
}
