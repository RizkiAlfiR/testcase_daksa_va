import 'package:testcase_daksa_va/model/data_history.dart';

class HistoryTransaction {
  List<DataHistory> data;
  int rowCount;

  HistoryTransaction({this.data, this.rowCount});

  HistoryTransaction.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<DataHistory>();
      json['data'].forEach((v) {
        data.add(new DataHistory.fromJson(v));
      });
    }
    rowCount = json['rowCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['rowCount'] = this.rowCount;
    return data;
  }
}
