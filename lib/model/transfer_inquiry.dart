class TransferInquiry {
  String inquiryId;
  String accountSrcId;
  String accountDstId;
  String accountSrcName;
  String accountDstName;
  double amount;

  TransferInquiry(
      {this.inquiryId,
      this.accountSrcId = "00",
      this.accountDstId = "00",
      this.accountSrcName = "000000",
      this.accountDstName = "000000",
      this.amount = 0});

  TransferInquiry.fromJson(Map<String, dynamic> json) {
    inquiryId = json['inquiryId'];
    accountSrcId = json['accountSrcId'];
    accountDstId = json['accountDstId'];
    accountSrcName = json['accountSrcName'];
    accountDstName = json['accountDstName'];
    amount = json['amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['inquiryId'] = this.inquiryId;
    data['accountSrcId'] = this.accountSrcId;
    data['accountDstId'] = this.accountDstId;
    data['accountSrcName'] = this.accountSrcName;
    data['accountDstName'] = this.accountDstName;
    data['amount'] = this.amount;
    return data;
  }
}
