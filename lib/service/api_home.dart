import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:testcase_daksa_va/model/user_detail.dart';
import 'package:testcase_daksa_va/model/user_login.dart';

import 'url_global.dart' as url_global;

class ApiHome {
  Future<UserDetail> userDetail({@required UserLogin userLogin}) async {
    try {
      var dio = await url_global.dio();

      Response<String> response = await dio.get(
        "rest/account/detail",
        queryParameters: {
          "id": userLogin.accountId,
        },
        options: Options(headers: {"_sessionId": userLogin.id}),
      );
      // List jsonUser = jsonDecode(response.data);
      // UserDetail userDetail = jsonDecode(response.data);
      UserDetail userDetail = UserDetail.fromJson(jsonDecode(response.data));

      return userDetail;
    } on DioError catch (e) {
      if (e.response != null) {
        // var json = jsonDecode(e.response.data);
        var json = e.response.data;
        throw "$json";
      } else {
        throw "Error Connection";
      }
    }
  }
}
