import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

import 'url_global.dart' as url_global;

class ApiAuth {
  Future<String> userLogin(
      {@required String username, @required String password}) async {
    try {
      var dio = await url_global.dio();

      Response<String> response = await dio.post(
        "rest/auth/login",
        data: {
          "username": username.trim(),
          "password": password.trim(),
        },
      );
      // UserLogin userLogin = UserLogin();
      // var json = jsonDecode(response.data);
      var json = response.data;

      return json;
    } on DioError catch (e) {
      if (e.response != null) {
        // var json = jsonDecode(e.response.data);
        var json = e.response.data;
        throw "$json";
      } else {
        throw "Error Connection";
      }
    }
  }
}
