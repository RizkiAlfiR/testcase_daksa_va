import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:testcase_daksa_va/model/data_history.dart';
import 'package:testcase_daksa_va/model/transfer_inquiry.dart';
import 'package:testcase_daksa_va/model/user_login.dart';

import 'url_global.dart' as url_global;

class ApiTransfer {
  Future<TransferInquiry> transferInquiry({
    @required String accountDest,
    @required String amount,
    @required UserLogin userLogin,
  }) async {
    try {
      var dio = await url_global.dio();

      Response<String> response = await dio.post(
        "rest/account/transaction/transferInquiry",
        data: {
          "accountSrcId": userLogin.accountId.trim(),
          "accountDstId": accountDest.trim(),
          "amount": amount,
        },
        options: Options(headers: {"_sessionId": userLogin.id}),
      );
      TransferInquiry transferInquiry =
          TransferInquiry.fromJson(jsonDecode(response.data));

      return transferInquiry;
    } on DioError catch (e) {
      if (e.response != null) {
        // var json = jsonDecode(e.response.data);
        var json = e.response.data;
        throw "$json";
      } else {
        throw "Error Connection";
      }
    }
  }

  Future<DataHistory> transferConfirm({
    @required TransferInquiry inquiry,
    @required UserLogin userLogin,
  }) async {
    try {
      var dio = await url_global.dio();

      Response<String> response = await dio.post(
        "rest/account/transaction/transfer",
        data: {
          "accountSrcId": inquiry.accountSrcId.trim(),
          "accountDstId": inquiry.accountDstId.trim(),
          "amount": inquiry.amount,
          "inquiryId": inquiry.inquiryId.trim(),
        },
        options: Options(headers: {"_sessionId": userLogin.id}),
      );
      DataHistory dataHistory = DataHistory.fromJson(jsonDecode(response.data));

      return dataHistory;
    } on DioError catch (e) {
      if (e.response != null) {
        // var json = jsonDecode(e.response.data);
        var json = e.response.data;
        throw "$json";
      } else {
        throw "Error Connection";
      }
    }
  }
}
