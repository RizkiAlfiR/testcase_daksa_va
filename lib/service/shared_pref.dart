import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:testcase_daksa_va/model/user_login.dart';

final String _keyUser = "keyuser";
final String _keySession = "keySession";

class SharedPref {
  Future<bool> addUser(String user) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    // Map json = user.toJson();
    // String string = jsonEncode(json);
    // String string = user.toString();

    return prefs.setString(_keyUser, user);
  }

  Future<UserLogin> getUser() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      Map stringUser = jsonDecode(prefs.getString(_keyUser));
      UserLogin user = new UserLogin.fromJson(stringUser);

      return user;
    } catch (e) {
      return null;
    }
  }
// Future<bool> addSession(String sessionId) async {
//   final SharedPreferences prefs = await SharedPreferences.getInstance();
//   print("====HAALOOO ${sessionId}");
//
//   return prefs.setString(_keySession, sessionId);
// }
}
