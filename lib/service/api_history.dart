import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:testcase_daksa_va/model/history_transaction.dart';
import 'package:testcase_daksa_va/model/user_login.dart';

import 'url_global.dart' as url_global;

class ApiHistory {
  Future<HistoryTransaction> historyLog({@required UserLogin userLogin}) async {
    try {
      var dio = await url_global.dio();

      Response<String> response = await dio.get(
        "rest/account/transaction/log",
        queryParameters: {
          "accountSrcId": userLogin.accountId,
        },
        options: Options(headers: {"_sessionId": userLogin.id}),
      );

      HistoryTransaction historyTransaction =
          HistoryTransaction.fromJson(jsonDecode(response.data));

      return historyTransaction;
    } on DioError catch (e) {
      if (e.response != null) {
        // var json = jsonDecode(e.response.data);
        var json = e.response.data;
        throw "$json";
      } else {
        throw "Error Connection";
      }
    }
  }
}
