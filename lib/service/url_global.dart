import 'package:dio/dio.dart';

var globalUrl = "https://mockva.daksa.co.id/mockva-rest";

Dio dioInstance;

createInstance() async {
  var options = BaseOptions(
    baseUrl: "$globalUrl/",
    connectTimeout: 120000,
    receiveTimeout: 120000,
    // headers: {
    //   "x-api-key": apiKey,
    // },
  );
  dioInstance = new Dio(options);

  dioInstance.interceptors.add(LogInterceptor(
    error: true,
    responseBody: true,
    request: true,
    requestBody: true,
    requestHeader: true,
  ));
}

Future<Dio> dio() async {
  if (dioInstance == null) {
    await createInstance();
  }

  ///TODO: WIP pref User
  // var user = await pref.getUser();

  // if (user != null) {
  ///TODO: WIP sessionID
  //   dioInstance.options.headers['Authorization'] = "Bearer ${user.token}";
  // } else {
  //   dioInstance.options.headers.remove("Authorization");
  // }
  dioInstance.options.baseUrl = "$globalUrl/";

  return dioInstance;
}
