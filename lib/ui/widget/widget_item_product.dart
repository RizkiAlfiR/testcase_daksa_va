import 'package:flutter/material.dart';

class WidgetItemProduct extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(5.0),
                  child: Image.asset(
                    "assets/images/dummy_converse.jpeg",
                    width: MediaQuery.of(context).size.width / 4,
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 2,
                  margin: EdgeInsets.only(left: 15.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Text(
                        "Sepatu Converse Chuck Taylor, Black and White",
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 12.0,
                        ),
                      ),
                      SizedBox(height: 5.0),
                      Text(
                        "IDR 450,000",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 12.0,
                        ),
                      ),
                      SizedBox(height: 15.0),
                      Row(
                        children: [
                          Text(
                            "Stok: 20",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12.0,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 10.0),
                            padding: EdgeInsets.symmetric(
                                horizontal: 7.0, vertical: 2.0),
                            color: Color(0xffD6FFDE),
                            child: Text(
                              "Aktif",
                              style: TextStyle(
                                color: Color(0xff03AC0E),
                                fontSize: 12.0,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 5.0),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5.0),
            child: Row(children: [
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 10.0),
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Color(0xff7A7D80),
                      width: 1.0,
                    ),
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Text(
                    "Ubah Harga",
                    style: TextStyle(
                      color: Color(0xff6C727C),
                      fontWeight: FontWeight.w600,
                      fontSize: 12.0,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 10.0),
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Color(0xff7A7D80),
                      width: 1.0,
                    ),
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Text(
                    "Ubah Stok",
                    style: TextStyle(
                      color: Color(0xff6C727C),
                      fontWeight: FontWeight.w600,
                      fontSize: 12.0,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }
}
