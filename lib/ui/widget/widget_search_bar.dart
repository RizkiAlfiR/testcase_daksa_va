import 'package:flutter/material.dart';

class WidgetSearchBar extends StatefulWidget {
  @override
  _WidgetSearchBarState createState() => _WidgetSearchBarState();
}

class _WidgetSearchBarState extends State<WidgetSearchBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey,
      width: MediaQuery.of(context).size.width,
      height: 30.0,
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 15.0),
      margin: EdgeInsets.symmetric(horizontal: 15.0),
      child: Text("WIP Nambah Search Bar"),
    );
  }
}
