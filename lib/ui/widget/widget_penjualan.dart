import 'package:flutter/material.dart';

class WidgetPenjualan extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 15.0),
                  child: Text(
                    "Penjualan",
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 15.0),
                  child: Text(
                    "Lihat Riwayat",
                    textAlign: TextAlign.right,
                    style: TextStyle(color: Color(0xff03AC0E)),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 5.0 * 4,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15.0),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 15.0),
                    child: Column(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(5.0),
                          child: Image.asset(
                            "assets/images/ic_pesanan_baru.png",
                            width: MediaQuery.of(context).size.width / 8,
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                        SizedBox(
                          height: 5.0 * 2,
                        ),
                        Text(
                          "Pesanan Baru",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 12.0),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 15.0),
                    child: Column(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(5.0),
                          child: Image.asset(
                            "assets/images/ic_siap_kirim.png",
                            width: MediaQuery.of(context).size.width / 8,
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                        SizedBox(
                          height: 5.0 * 2,
                        ),
                        Text(
                          "Siap Dikirim",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 12.0),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 15.0),
                    child: Column(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(5.0),
                          child: Image.asset(
                            "assets/images/ic_sedang_dikirim.png",
                            width: MediaQuery.of(context).size.width / 8,
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                        SizedBox(
                          height: 5.0 * 2,
                        ),
                        Text(
                          "Sedang Dikirim",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 12.0),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 15.0),
                    child: Column(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(5.0),
                          child: Image.asset(
                            "assets/images/ic_sampai_tujuan.png",
                            width: MediaQuery.of(context).size.width / 8,
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                        SizedBox(
                          height: 5.0 * 2,
                        ),
                        Text(
                          "Sampai Tujuan",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 12.0),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 5.0,
          ),
          Container(
            height: 2.0,
            color: Color(0xfff2f2f2),
          ),
        ],
      ),
    );
  }
}
