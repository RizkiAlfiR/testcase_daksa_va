import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class WidgetPageTitle extends StatefulWidget {
  final bool isBackButton;
  final String title;

  const WidgetPageTitle(
      {Key key, this.isBackButton = true, @required this.title})
      : super(key: key);

  @override
  _WidgetPageTitleState createState() => _WidgetPageTitleState();
}

class _WidgetPageTitleState extends State<WidgetPageTitle> {
  @override
  Widget build(BuildContext context) {
    var screen = MediaQuery.of(context).size;

    return Container(
      child: InkWell(
        onTap: () {
          Navigator.pop(context);
        },
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            children: <Widget>[
//              Container(
//                padding: EdgeInsets.only(left: 0, top: 10, bottom: 10),
//                child: Icon(Icons.keyboard_arrow_left, color: Colors.black),
//              ),
              widget.isBackButton
                  ? Align(
                      alignment: Alignment.centerLeft,
                      child: new Icon(
                        Icons.arrow_back_ios,
                        color: Colors.grey,
                        size: screen.width / 82.2 * 4,
                      ),
                    )
                  : Container(),
              Text(
                widget.title,
//                style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                style: GoogleFonts.portLligatSans(),
              )
            ],
          ),
        ),
      ),
    );
  }
}
