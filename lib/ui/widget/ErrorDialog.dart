import 'package:flutter/material.dart';

class ErrorDialog {
  final BuildContext context;

  String title;
  String message;

  List<Widget> actions;

  ErrorDialog(this.context,
      {this.title, this.message, this.actions = const []});

  ErrorDialog addAction(Widget action) {
    actions.add(action);
    return this;
  }

  ErrorDialog setTitle(String title) {
    this.title = title;
    return this;
  }

  ErrorDialog setMessage(String message) {
    this.message = message;
    return this;
  }

  show() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text((this.title == null) ? "Whoops" : this.title),
          content: Text(this.message),
          actions: <Widget>[
            this.actions.isEmpty
                ? FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text("Close"),
                  )
                : this.actions.map((widget) => widget).toList(),
          ],
        );
      },
    );
  }
}
