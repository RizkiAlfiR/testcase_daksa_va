import 'package:flutter/material.dart';
import 'package:testcase_daksa_va/ui/page/daftar_produk_page.dart';
import 'package:testcase_daksa_va/ui/page/tambah_produk_page.dart';
import 'package:testcase_daksa_va/ui/widget/widget_header_home.dart';
import 'package:testcase_daksa_va/ui/widget/widget_penjualan.dart';

class TabHome extends StatefulWidget {
  @override
  _TabHomeState createState() => _TabHomeState();
}

class _TabHomeState extends State<TabHome> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        WidgetHeaderHome(),
        SizedBox(
          height: 20.0,
        ),
        WidgetPenjualan(),
        SizedBox(
          height: 5.0 * 4,
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 15.0),
          child: Text(
            "Produk",
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        SizedBox(
          height: 5.0 * 2,
        ),
        FlatButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => new TambahProdukPage()),
            );
          },
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(vertical: 12.0),
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey,
                width: 1.0,
              ),
              borderRadius: BorderRadius.circular(5.0),
            ),
            child: Text(
              "+ Tambah Produk",
              style: TextStyle(
                color: Color(0xff6C727C),
                fontWeight: FontWeight.w400,
                fontSize: 12.0,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        SizedBox(
          height: 5.0 * 2,
        ),
        FlatButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => new DaftarProdukPage()),
            );
          },
          child: Row(
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.6,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(
                      child: Text(
                        "Produk Anda",
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Container(
                      child: Text(
                        "Lihat dan atur semua Produk",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Color(0xff6C727C),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                width: MediaQuery.of(context).size.width * 0.3,
                child: Icon(
                  Icons.arrow_forward_ios,
                  size: 15.0,
                  color: Color(0xff6C727C),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 5.0 * 2,
        ),
        Container(
          height: 2.0,
          color: Color(0xfff2f2f2),
        ),
        SizedBox(
          height: 5.0 * 2,
        ),
        FlatButton(
          onPressed: () {
            print("HAIII");
          },
          child: Row(
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.6,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(
                      child: Text(
                        "Draf Produk",
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Container(
                      child: Text(
                        "Selesaikan kelengkapan detail Produk",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Color(0xff6C727C),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                width: MediaQuery.of(context).size.width * 0.3,
                child: Icon(
                  Icons.arrow_forward_ios,
                  size: 15.0,
                  color: Color(0xff6C727C),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 5.0 * 2,
        ),
        Container(
          height: 2.0,
          color: Color(0xfff2f2f2),
        ),
        SizedBox(
          height: 5.0 * 2,
        ),
        FlatButton(
          onPressed: () {
            print("HAIII");
          },
          child: Row(
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.6,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(
                      child: Text(
                        "Pusat Panduan Seller",
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Container(
                      child: Text(
                        "Edukasi dan beragam panduan untuk penjualan anda",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Color(0xff6C727C),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                width: MediaQuery.of(context).size.width * 0.3,
                child: Icon(
                  Icons.arrow_forward_ios,
                  size: 15.0,
                  color: Color(0xff6C727C),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 5.0 * 2,
        ),
        Container(
          height: 2.0,
          color: Color(0xfff2f2f2),
        ),
      ],
    ));
  }
}
