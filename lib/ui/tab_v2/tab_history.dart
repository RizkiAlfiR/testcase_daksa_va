import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:testcase_daksa_va/model/history_transaction.dart';
import 'package:testcase_daksa_va/model/user_login.dart';
import 'package:testcase_daksa_va/service/api_history.dart';
import 'package:testcase_daksa_va/service/shared_pref.dart';
import 'package:testcase_daksa_va/ui/widget/widget_item_transaksi.dart';

class TabHistory extends StatefulWidget {
  @override
  _TabHistoryState createState() => _TabHistoryState();
}

class _TabHistoryState extends State<TabHistory> {
  UserLogin userLogin = UserLogin();
  HistoryTransaction historyTransaction = HistoryTransaction();
  bool isLoading;
  ScrollController scrollController = new ScrollController();

  @override
  void initState() {
    setState(() {
      isLoading = true;
    });
    SharedPref().getUser().then((value) {
      userLogin = value;
    }).whenComplete(() => fetchHistory());
    super.initState();
  }

  Future<void> fetchHistory() async {
    historyTransaction =
        await ApiHistory().historyLog(userLogin: userLogin).whenComplete(() {
      setState(() {
        isLoading = false;
      });
    });

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        controller: scrollController,
        physics: ClampingScrollPhysics(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            SizedBox(
              height: 5.0 * 4,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15.0),
              child: Text(
                "History Transaksi",
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            SizedBox(
              height: 5.0 * 2,
            ),
            isLoading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    itemCount: historyTransaction.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      return WidgetItemTransaksi(
                        dataHistory: historyTransaction.data[index],
                      );
                    },
                  ),
            SizedBox(
              height: 5.0 * 2,
            ),
          ],
        ),
      ),
    );
  }
}
