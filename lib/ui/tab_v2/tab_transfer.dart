import 'package:flutter/material.dart';
import 'package:testcase_daksa_va/model/user_login.dart';
import 'package:testcase_daksa_va/service/api_transfer.dart';
import 'package:testcase_daksa_va/service/shared_pref.dart';
import 'package:testcase_daksa_va/ui/page/transfer_detail_page.dart';
import 'package:testcase_daksa_va/ui/widget/ErrorDialog.dart';

class TabTransfer extends StatefulWidget {
  @override
  _TabTransferState createState() => _TabTransferState();
}

class _TabTransferState extends State<TabTransfer> {
  TextEditingController edtAccDst = new TextEditingController();
  TextEditingController edtAmount = new TextEditingController();
  FocusNode fnAccDst = new FocusNode();
  FocusNode fnAmount = new FocusNode();
  UserLogin userLogin = new UserLogin();

  @override
  void initState() {
    SharedPref().getUser().then((value) {
      userLogin = value;
    });
    super.initState();
  }

  attemptToTransfer() {
    if (edtAccDst.text.isNotEmpty && edtAmount.text.isNotEmpty) {
      ApiTransfer()
          .transferInquiry(
              accountDest: edtAccDst.text,
              amount: edtAmount.text,
              userLogin: userLogin)
          .then((transfer) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => new TransferDetailPage(
              transferInquiry: transfer,
            ),
          ),
        );
        new ErrorDialog(context).setMessage(transfer.toString()).show();
      }).catchError((message) {
        if (message != "Error Connection") {
          new ErrorDialog(context).setMessage(message.toString()).show();
        } else {
          // Navigator.pop(context);
          new ErrorDialog(context).setMessage("Error Connection").show();
        }
      });
    } else {
      new ErrorDialog(context).setMessage("Form harus diisi.").show();
    }
  }

  Widget _emailPasswordWidget() {
    return Column(
      children: <Widget>[
        _entryField("Account Destination", edtAccDst),
        _entryField("Amount", edtAmount),
      ],
    );
  }

  Widget _entryField(String title, TextEditingController controller,
      {bool isPassword = false}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            controller: controller,
            obscureText: isPassword,
            decoration: InputDecoration(
                border: InputBorder.none,
                // border: new OutlineInputBorder(
                //     borderSide: new BorderSide(color: Colors.orange)),
                fillColor: Color(0xfff3f3f4),
                filled: true),
            validator: (value) {
              if (value.isEmpty) {
                return "Harus terisi";
              }

              return null;
            },
          ),
        ],
      ),
    );
  }

  Widget _submitButton() {
    return GestureDetector(
      onTap: () {
        attemptToTransfer();
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 15),
        margin: EdgeInsets.symmetric(horizontal: 35),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey.shade200,
                  offset: Offset(2, 4),
                  blurRadius: 5,
                  spreadRadius: 2)
            ],
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [Color(0xfffbb448), Color(0xffe46b10)],
            )),
        child: Text(
          'Transfer',
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final screen = MediaQuery.of(context).size;

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(
            height: 120.0,
          ),
          _emailPasswordWidget(),
          SizedBox(height: 20),
          _submitButton(),
        ],
      ),
    );
  }
}
