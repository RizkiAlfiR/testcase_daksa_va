import 'package:flutter/material.dart';
import 'package:testcase_daksa_va/model/user_detail.dart';
import 'package:testcase_daksa_va/model/user_login.dart';
import 'package:testcase_daksa_va/service/api_home.dart';
import 'package:testcase_daksa_va/service/shared_pref.dart';
import 'package:testcase_daksa_va/ui/widget/widget_header_home.dart';

class TabHome extends StatefulWidget {
  @override
  _TabHomeState createState() => _TabHomeState();
}

class _TabHomeState extends State<TabHome> {
  UserLogin userLogin = UserLogin();
  UserDetail userDetail = UserDetail();

  @override
  void initState() {
    SharedPref().getUser().then((value) {
      userLogin = value;
    }).whenComplete(() => fetchUser());
    super.initState();
  }

  Future<void> fetchUser() async {
    userDetail = await ApiHome().userDetail(userLogin: userLogin);

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        WidgetHeaderHome(
          userDetail: userDetail,
        ),
        SizedBox(
          height: 20.0,
        ),
        // WidgetPenjualan(),
        SizedBox(
          height: 5.0 * 4,
        ),
      ],
    ));
  }
}
