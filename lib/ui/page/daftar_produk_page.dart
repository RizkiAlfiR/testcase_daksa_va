import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testcase_daksa_va/ui/page/tambah_produk_page.dart';
import 'package:testcase_daksa_va/ui/widget/widget_item_product.dart';
import 'package:testcase_daksa_va/ui/widget/widget_search_bar.dart';

class DaftarProdukPage extends StatefulWidget {
  @override
  _DaftarProdukPageState createState() => _DaftarProdukPageState();
}

class _DaftarProdukPageState extends State<DaftarProdukPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Daftar Produk",
          style: GoogleFonts.portLligatSans(),
        ),
        backgroundColor: Colors.orangeAccent,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => new TambahProdukPage()),
                );
              }),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(bottom: 15.0),
          child: Column(
//          mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              SizedBox(
                height: 5.0 * 3,
              ),
              WidgetSearchBar(),
              SizedBox(
                height: 5.0 * 2,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 15.0),
                child: Text(
                  "15 Produk",
                  textAlign: TextAlign.left,
                ),
              ),
              SizedBox(
                height: 5.0 * 3,
              ),
              Container(
                height: 2.0,
                color: Color(0xfff2f2f2),
              ),
              WidgetItemProduct(),
              Container(
                height: 10.0,
                color: Color(0xfff2f2f2),
              ),
              WidgetItemProduct(),
              Container(
                height: 10.0,
                color: Color(0xfff2f2f2),
              ),
              WidgetItemProduct(),
              Container(
                height: 10.0,
                color: Color(0xfff2f2f2),
              ),
              WidgetItemProduct(),
              Container(
                height: 10.0,
                color: Color(0xfff2f2f2),
              ),
              WidgetItemProduct(),
              Container(
                height: 10.0,
                color: Color(0xfff2f2f2),
              ),
              WidgetItemProduct(),
              Container(
                height: 10.0,
                color: Color(0xfff2f2f2),
              ),
              WidgetItemProduct(),
              Container(
                height: 10.0,
                color: Color(0xfff2f2f2),
              ),
              WidgetItemProduct(),
              Container(
                height: 10.0,
                color: Color(0xfff2f2f2),
              ),
              WidgetItemProduct(),
              Container(
                height: 10.0,
                color: Color(0xfff2f2f2),
              ),
              WidgetItemProduct(),
              Container(
                height: 10.0,
                color: Color(0xfff2f2f2),
              ),
              WidgetItemProduct(),
              Container(
                height: 10.0,
                color: Color(0xfff2f2f2),
              ),
              WidgetItemProduct(),
              Container(
                height: 10.0,
                color: Color(0xfff2f2f2),
              ),
              WidgetItemProduct(),
              Container(
                height: 10.0,
                color: Color(0xfff2f2f2),
              ),
              WidgetItemProduct(),
              Container(
                height: 10.0,
                color: Color(0xfff2f2f2),
              ),
              WidgetItemProduct(),
              Container(
                height: 10.0,
                color: Color(0xfff2f2f2),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
