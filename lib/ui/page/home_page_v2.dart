import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testcase_daksa_va/model/user_detail.dart';
import 'package:testcase_daksa_va/model/user_login.dart';
import 'package:testcase_daksa_va/ui/tab_v2/tab_account.dart';
import 'package:testcase_daksa_va/ui/tab_v2/tab_history.dart';
import 'package:testcase_daksa_va/ui/tab_v2/tab_home.dart';
import 'package:testcase_daksa_va/ui/tab_v2/tab_transfer.dart';
import 'package:testcase_daksa_va/ui/widget/bezierContainer.dart';

class HomePageV2 extends StatefulWidget {
  @override
  _HomePageV2State createState() => _HomePageV2State();
}

class _HomePageV2State extends State<HomePageV2> {
  int _selectedTabIndex = 0;
  UserLogin userLogin = UserLogin();
  UserDetail userDetail = UserDetail();

  @override
  void initState() {
    super.initState();
  }

  void onNavbarTapped(int index) {
    setState(() {
      _selectedTabIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final listPage = <Widget>[
      TabHome(),
      TabTransfer(),
      TabHistory(),
      TabAccount(),
    ];

    final _bottomNavbarItems = <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(Icons.home),
        title: Text(
          "Home",
          style: GoogleFonts.portLligatSans(),
        ),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.transform),
        title: Text(
          "Transfer",
          style: GoogleFonts.portLligatSans(),
        ),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.history),
        title: Text(
          "History",
          style: GoogleFonts.portLligatSans(),
        ),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.settings),
        title: Text(
          "Account",
          style: GoogleFonts.portLligatSans(),
        ),
      )
    ];

    final _bottomNavbar = BottomNavigationBar(
      items: _bottomNavbarItems,
      currentIndex: _selectedTabIndex,
      selectedItemColor: Colors.orangeAccent,
      unselectedItemColor: Colors.grey,
      onTap: onNavbarTapped,
    );

    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Mock VA Mobile",
          style: GoogleFonts.portLligatSans(),
        ),
        backgroundColor: Colors.orangeAccent,
      ),
      body: Container(
        height: height,
        child: SafeArea(
          child: Stack(
            children: <Widget>[
              Positioned(
                  top: -height * .15,
                  right: -MediaQuery.of(context).size.width * .4,
                  child: BezierContainer()),
              Container(
                child: SingleChildScrollView(
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.symmetric(vertical: 15),
                      alignment: Alignment.center,
                      child: listPage[_selectedTabIndex]),
                ),
              ),
//              Positioned(
//                top: 40,
//                left: 0,
//                child: WidgetPageTitle(
//                  title: "Initial D",
//                ),
//              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: _bottomNavbar,
    );
  }
}
