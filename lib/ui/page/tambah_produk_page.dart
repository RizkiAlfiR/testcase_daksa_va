import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';

class TambahProdukPage extends StatefulWidget {
  @override
  _TambahProdukPageState createState() => _TambahProdukPageState();
}

class _TambahProdukPageState extends State<TambahProdukPage> {
  final TextEditingController _judulProdukController = TextEditingController();
  final TextEditingController _deskripsiProdukController =
      TextEditingController();
  final TextEditingController _hargaProdukController = TextEditingController();
  final TextEditingController _stockProdukController = TextEditingController();
  final GlobalKey<FormFieldState> _judulProdukFormKey =
      GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> _deskripsiProdukFormKey =
      GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> _hargaProdukFormKey =
      GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> _stockProdukFormKey =
      GlobalKey<FormFieldState>();
  bool _isSubmitButtonEnabled = false;
  File _fotoProduk;

  @override
  void initState() {
    super.initState();

//    _judulProdukController.addListener(_printLatestValue);
  }

  @override
  void dispose() {
    _judulProdukController.dispose();
    _deskripsiProdukController.dispose();
    _hargaProdukController.dispose();
    _stockProdukController.dispose();
    super.dispose();
  }

  _imgFromCamera() async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50);

    setState(() {
      _fotoProduk = image;
    });
  }

  _imgFromGallery() async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50);

    setState(() {
      _fotoProduk = image;
    });
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  bool _isFormValid() {
    return ((_judulProdukFormKey.currentState.isValid &&
        _deskripsiProdukFormKey.currentState.isValid &&
        _hargaProdukFormKey.currentState.isValid &&
        _stockProdukFormKey.currentState.isValid));
  }

  void _submit() {
    print('judul: ' + _judulProdukController.text);
    print('deskripsi: ' + _deskripsiProdukController.text);
    print('harga: ' + _hargaProdukController.text);
    print('stock: ' + _stockProdukController.text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Tambah Produk",
          style: GoogleFonts.portLligatSans(),
        ),
        backgroundColor: Colors.orangeAccent,
      ),
      body: Container(
        margin: EdgeInsets.symmetric(
          horizontal: 15.0,
          vertical: 15.0,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: "Foto Produk",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    TextSpan(
                      text: " *",
                      style: TextStyle(
                        color: Colors.red,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 5.0 * 2,
              ),
              Container(
                child: GestureDetector(
                  onTap: () {
                    _showPicker(context);
                  },
                  child: _fotoProduk != null
                      ? Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(color: Colors.orangeAccent),
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Image.file(
                              _fotoProduk,
                              width: 100,
                              height: 100,
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                        )
                      : Container(
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(color: Colors.orangeAccent),
                          ),
                          width: 100,
                          height: 100,
                          child: Icon(
                            Icons.camera_alt,
                            color: Colors.grey[800],
                          ),
                        ),
                ),
              ),
              SizedBox(
                height: 5.0 * 4,
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: "Isi nama produk yang akan dijual",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    TextSpan(
                      text: " *",
                      style: TextStyle(
                        color: Colors.red,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              TextFormField(
                minLines: 1,
                maxLines: 3,
                maxLength: 20,
                controller: _judulProdukController,
                textInputAction: TextInputAction.next,
                key: _judulProdukFormKey,
                decoration: const InputDecoration(
                  labelText: 'Nama Produk',
                ),
                onChanged: (value) {
                  setState(() {
                    _judulProdukFormKey.currentState.validate();
                    _isSubmitButtonEnabled = _isFormValid();
                  });
                },
                validator: (value) {
                  if (value.length < 5) {
                    return 'Tips: Jenis Produk + Merk Produk + Keterangan Tambahan.';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 5.0 * 4,
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: "Deskripsikan produk yang kamu jual",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    TextSpan(
                      text: " *",
                      style: TextStyle(
                        color: Colors.red,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              TextFormField(
                minLines: 1,
                maxLines: 3,
                maxLength: 70,
                controller: _deskripsiProdukController,
                textInputAction: TextInputAction.next,
                key: _deskripsiProdukFormKey,
                decoration: const InputDecoration(
                  labelText: 'Deskripsi Produk',
                ),
                onChanged: (value) {
                  setState(() {
                    _deskripsiProdukFormKey.currentState.validate();
                    _isSubmitButtonEnabled = _isFormValid();
                  });
                },
                validator: (value) {
                  if (value.length < 15) {
                    return 'Tips: Jenis Produk + Merk Produk + Keterangan Tambahan.';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 5.0 * 4,
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: "Harga",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    TextSpan(
                      text: " *",
                      style: TextStyle(
                        color: Colors.red,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              TextFormField(
                minLines: 1,
                maxLines: 1,
                maxLength: 20,
                controller: _hargaProdukController,
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.number,
                key: _hargaProdukFormKey,
                decoration: const InputDecoration(
                  labelText: 'Harga Produk',
                ),
                onChanged: (value) {
                  setState(() {
                    _hargaProdukFormKey.currentState.validate();
                    _isSubmitButtonEnabled = _isFormValid();
                  });
                },
                validator: (value) {
                  if (int.parse(value) < 10000) {
                    return 'Harga produk minimum adalah Rp 10,000.';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 5.0 * 4,
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: "Stock",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    TextSpan(
                      text: " *",
                      style: TextStyle(
                        color: Colors.red,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              TextFormField(
                minLines: 1,
                maxLines: 1,
                maxLength: 10,
                controller: _stockProdukController,
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.number,
                key: _stockProdukFormKey,
                decoration: const InputDecoration(
                  labelText: 'Stock Produk',
                ),
                onChanged: (value) {
                  setState(() {
                    _stockProdukFormKey.currentState.validate();
                    _isSubmitButtonEnabled = _isFormValid();
                  });
                },
                validator: (value) {
                  if (int.parse(value) < 1) {
                    return 'Stock minumum dari produk kamu adalah 1.';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 5.0 * 4,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: RaisedButton(
                  color: Colors.orangeAccent,
                  textColor: Colors.white,
                  onPressed: _isSubmitButtonEnabled ? () => _submit() : null,
                  child: const Text(
                    'Simpan',
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
