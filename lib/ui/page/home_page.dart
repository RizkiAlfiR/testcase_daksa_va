import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testcase_daksa_va/ui/page/welcome_page.dart';
import 'package:testcase_daksa_va/ui/tab/tab_home.dart';
import 'package:testcase_daksa_va/ui/widget/bezierContainer.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedTabIndex = 0;

  void onNavbarTapped(int index) {
    setState(() {
      _selectedTabIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final listPage = <Widget>[
      TabHome(),
      Text("Halaman Order"),
      Text("Halaman Notifikasi"),
      Text("Halaman Akun"),
    ];

    final _bottomNavbarItems = <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(Icons.home),
        title: Text(
          "Home",
          style: GoogleFonts.portLligatSans(),
        ),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.shopping_cart),
        title: Text(
          "Order Masuk",
          style: GoogleFonts.portLligatSans(),
        ),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.notifications),
        title: Text(
          "Notifikasi",
          style: GoogleFonts.portLligatSans(),
        ),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.person),
        title: Text(
          "Akun",
          style: GoogleFonts.portLligatSans(),
        ),
      )
    ];

    final _bottomNavbar = BottomNavigationBar(
      items: _bottomNavbarItems,
      currentIndex: _selectedTabIndex,
      selectedItemColor: Colors.orangeAccent,
      unselectedItemColor: Colors.grey,
      onTap: onNavbarTapped,
    );

    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Initial D",
          style: GoogleFonts.portLligatSans(),
        ),
        backgroundColor: Colors.orangeAccent,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => new WelcomePage()),
                );
              }),
        ],
      ),
      body: Container(
        height: height,
        child: SafeArea(
          child: Stack(
            children: <Widget>[
              Positioned(
                  top: -height * .15,
                  right: -MediaQuery.of(context).size.width * .4,
                  child: BezierContainer()),
              Container(
                child: SingleChildScrollView(
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.symmetric(vertical: 15),
                      alignment: Alignment.center,
                      child: listPage[_selectedTabIndex]),
                ),
              ),
//              Positioned(
//                top: 40,
//                left: 0,
//                child: WidgetPageTitle(
//                  title: "Initial D",
//                ),
//              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: _bottomNavbar,
    );
  }
}
