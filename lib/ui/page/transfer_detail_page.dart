import 'package:flutter/material.dart';
import 'package:testcase_daksa_va/model/transfer_inquiry.dart';
import 'package:testcase_daksa_va/model/user_login.dart';
import 'package:testcase_daksa_va/service/api_transfer.dart';
import 'package:testcase_daksa_va/service/shared_pref.dart';
import 'package:testcase_daksa_va/ui/widget/ErrorDialog.dart';

class TransferDetailPage extends StatefulWidget {
  final TransferInquiry transferInquiry;

  const TransferDetailPage({Key key, this.transferInquiry}) : super(key: key);

  @override
  _TransferDetailPageState createState() => _TransferDetailPageState();
}

class _TransferDetailPageState extends State<TransferDetailPage> {
  TextEditingController edtAccSrc = new TextEditingController();
  TextEditingController edtAccSrcName = new TextEditingController();
  TextEditingController edtAccDst = new TextEditingController();
  TextEditingController edtAccDstName = new TextEditingController();
  TextEditingController edtAmount = new TextEditingController();
  UserLogin userLogin = new UserLogin();
  TransferInquiry inquiry = new TransferInquiry();

  @override
  void initState() {
    SharedPref().getUser().then((value) {
      userLogin = value;
    });
    inquiry.inquiryId = widget.transferInquiry.inquiryId;
    inquiry.accountSrcId = widget.transferInquiry.accountSrcId;
    inquiry.accountSrcName = widget.transferInquiry.accountSrcName;
    inquiry.accountDstId = widget.transferInquiry.accountDstId;
    inquiry.accountDstName = widget.transferInquiry.accountDstName;
    inquiry.amount = widget.transferInquiry.amount;
    super.initState();
  }

  Widget _emailPasswordWidget() {
    return Column(
      children: <Widget>[
        _entryField("Account Source", edtAccSrc, type: "accSrc"),
        _entryField("Account Source Name", edtAccSrcName, type: "accSrcName"),
        _entryField("Account Destination", edtAccDst, type: "accDest"),
        _entryField("Account Destination Name", edtAccDstName,
            type: "accDestName"),
        _entryField("Amount", edtAmount, type: "amount"),
      ],
    );
  }

  Widget _entryField(String title, TextEditingController controller,
      {bool isPassword = false, String type}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          type == "accSrc"
              ? TextFormField(
                  initialValue: "${widget.transferInquiry.accountSrcId}",
                  enabled: false,
                  obscureText: isPassword,
                  controller: controller,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      // border: new OutlineInputBorder(
                      //     borderSide: new BorderSide(color: Colors.orange)),
                      fillColor: Color(0xfff3f3f4),
                      filled: true),
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Harus terisi";
                    }

                    return null;
                  },
                )
              : type == "accSrcName"
                  ? TextFormField(
                      initialValue: "${widget.transferInquiry.accountSrcName}",
                      enabled: false,
                      obscureText: isPassword,
                      controller: controller,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          // border: new OutlineInputBorder(
                          //     borderSide: new BorderSide(color: Colors.orange)),
                          fillColor: Color(0xfff3f3f4),
                          filled: true),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Harus terisi";
                        }

                        return null;
                      },
                    )
                  : type == "accDest"
                      ? TextFormField(
                          initialValue:
                              "${widget.transferInquiry.accountDstId}",
                          enabled: false,
                          obscureText: isPassword,
                          controller: controller,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              // border: new OutlineInputBorder(
                              //     borderSide: new BorderSide(color: Colors.orange)),
                              fillColor: Color(0xfff3f3f4),
                              filled: true),
                          validator: (value) {
                            if (value.isEmpty) {
                              return "Harus terisi";
                            }

                            return null;
                          },
                        )
                      : type == "accDestName"
                          ? TextFormField(
                              initialValue:
                                  "${widget.transferInquiry.accountDstName}",
                              enabled: false,
                              obscureText: isPassword,
                              controller: controller,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  // border: new OutlineInputBorder(
                                  //     borderSide: new BorderSide(color: Colors.orange)),
                                  fillColor: Color(0xfff3f3f4),
                                  filled: true),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Harus terisi";
                                }

                                return null;
                              },
                            )
                          : TextFormField(
                              initialValue: "${widget.transferInquiry.amount}",
                              enabled: false,
                              obscureText: isPassword,
                              controller: controller,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  // border: new OutlineInputBorder(
                                  //     borderSide: new BorderSide(color: Colors.orange)),
                                  fillColor: Color(0xfff3f3f4),
                                  filled: true),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Harus terisi";
                                }

                                return null;
                              },
                            )
        ],
      ),
    );
  }

  attemptToConfirm() {
    ApiTransfer()
        .transferConfirm(inquiry: inquiry, userLogin: userLogin)
        .then((value) {
      new ErrorDialog(context, title: "Congratulation")
          .setMessage(
              "Successfully transfer ${value.amount} on ${value.transactionTimestamp}.")
          .show();
    }).catchError((message) {
      if (message != "Error Connection") {
        new ErrorDialog(context).setMessage(message.toString()).show();
      } else {
        // Navigator.pop(context);
        new ErrorDialog(context).setMessage("Error Connection").show();
      }
    });
  }

  Widget _submitButton() {
    return GestureDetector(
      onTap: () {
        attemptToConfirm();
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 15),
        margin: EdgeInsets.symmetric(horizontal: 35),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey.shade200,
                  offset: Offset(2, 4),
                  blurRadius: 5,
                  spreadRadius: 2)
            ],
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [Color(0xfffbb448), Color(0xffe46b10)],
            )),
        child: Text(
          'Confirm',
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }

  Widget _backButton() {
    return InkWell(
      onTap: () {
        Navigator.pop(context);
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 0, top: 10, bottom: 10),
              child: Icon(Icons.keyboard_arrow_left, color: Colors.black),
            ),
            Text('Back',
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500))
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                _emailPasswordWidget(),
                SizedBox(height: 20),
                _submitButton(),
              ],
            ),
          ),
          Positioned(
            top: 40,
            left: 0,
            child: _backButton(),
          ),
        ],
      ),
    );
  }
}
